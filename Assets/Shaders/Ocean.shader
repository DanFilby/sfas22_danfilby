Shader "Custom/Ocean"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _Glossiness("Smoothness", Range(0,1)) = 0.5
        _Metallic("Metallic", Range(0,1)) = 0.0

        //properties to alter wave shape combined in a vector 4
        _WaveA("Wave A (dir, steepness, wavelength)", Vector) = (1,0,0.5,10)
        _WaveB("Wave B", Vector) = (1,0,0.5,10)
        _WaveC("Wave C", Vector) = (1,0,0.5,10)

        _RealTime("Real Time", float) = 0
        //_WaveHeightMap("Wave Height Map",2D) = "White"{}

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows vertex:Vert

        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        float4 _WaveA, _WaveB, _WaveC;
        float _RealTime;

        //sampler2D _WaveHeightMap;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)

        float3 GerstnerWave(float4 wave, float3 p, inout float3 tangent, inout float3 binormal) {

            float steepness = wave.z;
            float wavelength = wave.w;

            //calculates frequency and amplitude with the set properties 
            float k = 2 * UNITY_PI / wavelength;                     //wave number
            float c = sqrt(9.8 / k);                                //phase speed from gravitational strength
            float2 d = normalize(wave.xy);                         //direction of the wave
            float f = k * (dot(d, p.xz) - c * _RealTime);           //frequency from all variables 
            float a = steepness / k;                             //amplitude from the steepness

            //calculate and set the tangent
            tangent += float3(
                -d.x * d.x * (steepness * sin(f)),
                d.x * (steepness * cos(f)),
                -d.x * d.y * (steepness * sin(f))
                );

            //calculate and set the binormal
            binormal += float3(
                -d.x * d.y * (steepness * sin(f)),
                d.y * (steepness * cos(f)),
                -d.y * d.y * (steepness * sin(f))
                );

            //return the result of this wave
            return float3(d.x * (a * cos(f)), a * sin(f), d.y * (a * cos(f)));
        }


        void Vert(inout appdata_full vertexData) 
        {
            //find the current vertex positon
            float3 gridPoint = vertexData.vertex.xyz;
            float3 p = gridPoint;

            float3 tangent = float3(1, 0, 0);
            float3 binormal = float3(0, 0, 1);

            //calculate the first wave's offset, tangent and binormal
            p += GerstnerWave(_WaveA, gridPoint, tangent, binormal);

            //add the second and third wave
            p += GerstnerWave(_WaveB, gridPoint, tangent, binormal);
            p += GerstnerWave(_WaveC, gridPoint, tangent, binormal);

            float3 normal = normalize(cross(binormal, tangent));

            //apply the position change to the vertex
            vertexData.vertex.xyz = p;
            vertexData.normal = normal;

        }


        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
