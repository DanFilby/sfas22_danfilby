using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public void Onplay()
    {
        SceneManager.LoadScene(1);

    }

    public void OnExit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }



}
