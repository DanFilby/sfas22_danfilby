using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public float TimeLimit;
    public TMPro.TMP_Text timeText;
    public TMPro.TMP_Text scoreText;

    public Board _board;

    public List<Image> stars;
    public Sprite achievedStarImg;

    public int score;

    public int totalOilNodes;
    private int oilCollected = 0;
    private int oilDiscovered = 0;
    private int starsGained = 0;

    //amount of score to get each star
    private List<int> scoreValues;

    void Start()
    {
        totalOilNodes = _board.NumberOfOilTiles;
        CalculateScoreValues();     
    }

    void Update()
    {
        RefreshTimer();
        RefreshStars();
    }

    private void CalculateScoreValues()
    {
        //50 for discovering and 100 for all the oil 
        int totalAvailable = totalOilNodes * 50 + totalOilNodes * 100;

        scoreValues = new List<int>();

        scoreValues.Add(totalAvailable / 5);              //20% for 1 star
        scoreValues.Add(totalAvailable / 2);             //50% for 2
        scoreValues.Add((int)(totalAvailable * 0.75));   //75% for 3
    }

    private void RefreshTimer()
    {
        float time = TimeLimit - Time.timeSinceLevelLoad;
        string formatted = UI.FormatTime(time);
        timeText.text = formatted;

        if(time <= 0)
        {
            FinishGame();
        }
    }

    private void FinishGame()
    {
        Score.score = score;
        Score.stars = starsGained;
        SceneManager.LoadScene(2);
    }

    private void RefreshStars()
    {
        for (int i = 0; i < scoreValues.Count; i++)
        {
            if(score >= scoreValues[i])
            {
                stars[i].sprite = achievedStarImg;
                stars[i].color = Color.white;
                starsGained = i + 1;
            }
        }
    }

    public void UpdateScore(int oilDiscovered, int oilCollected)
    {
        score = oilDiscovered * 50 + oilCollected;
        scoreText.text = $"Score {score}";
    }

}
