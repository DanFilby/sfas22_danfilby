using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Results : MonoBehaviour
{
    public List<Image> stars;
    public Sprite achievedStarImg;

    public TMPro.TMP_Text scoreText;

    void Start()
    {
        SetupStars();
    }


    private void SetupStars()
    {
        for (int i = 0; i < Score.stars; i++)
        {
            stars[i].sprite = achievedStarImg;
            stars[i].color = Color.white;
        }

        scoreText.text = $"Score {Score.score}";
    }

}


