using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Box : MonoBehaviour
{
    //colours for the number of bombs nearby
    [SerializeField] private Color[] DangerColors = new Color[8];
    //bomb image
    [SerializeField] private Image DangerImg;

    //text for number of bombs nearby
    private TMP_Text _textDisplay;
    private Button _button;

    //callback function to let the board know this box ha been clicked
    private Action<Box> _changeCallback;

    public int RowIndex { get; private set; }
    public int ColumnIndex { get; private set; }
    public int ID { get; private set; }
    public int DangerNearby { get; private set; }
    public bool IsDangerous { get; private set; }
    public bool IsActive { get { return _button != null && _button.interactable; } }

    public void Setup(int id, int row, int column)
    {
        ID = id;
        RowIndex = row;
        ColumnIndex = column;
    }

    public void Charge(int dangerNearby, bool danger, Action<Box> onChange)
    {
        _changeCallback = onChange;
        DangerNearby = dangerNearby;
        IsDangerous = danger;
        ResetState();
    }

    public void Reveal()
    {
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (_textDisplay != null)
        {
            _textDisplay.enabled = true;
        }
    }

    public void StandDown()
    {
        if (_button != null)
        {
            _button.interactable = false;
        }

        if (DangerImg != null)
        {
            DangerImg.enabled = false;
        }

        if (_textDisplay != null)
        {
            _textDisplay.enabled = false;
        }
    }

    //when player clicks on this box. disable button for no further use.
    //if bomb show the image or show number of bombs around. send update to board
    public void OnClick()
    {
        if(_button != null)
        {
            _button.interactable = false;
        }

        if(IsDangerous && DangerImg != null)
        {
            DangerImg.enabled = true;
        }
        else if(_textDisplay != null)
        {
            _textDisplay.enabled = true;
        }

        //tell board this box has been clicked
        _changeCallback?.Invoke(this);
    }

    private void Awake()
    {
        _textDisplay = GetComponentInChildren<TMP_Text>(true);
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);

        ResetState();
    }

    private void ResetState()
    {
        if (DangerImg != null)
        {
            DangerImg.enabled = false;
        }

        if (_textDisplay != null)
        {
            if (DangerNearby > 0)
            {
                _textDisplay.text = DangerNearby.ToString("D");
                _textDisplay.color = DangerColors[DangerNearby-1];
            }
            else
            {
                _textDisplay.text = string.Empty;
            }

            _textDisplay.enabled = false;
        }

        if (_button != null)
        {
            _button.interactable = true;
        }
    }
}
