using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storm : MonoBehaviour
{
    public float speed;

    public Vector2 windDirection = new Vector2(1,1);

    //when the storm hits a border it will bounce and these values will be adjusted to -1 or 1.
    //this allows the wind direction to be changed i.e not instantly turning around if one of the values has already been adjusted
    private Vector2 boderCorrection = new Vector2(1,1);

    private Vector3 stormHalfSize;

    public OceanArea area;

    //represent thunder
    public List<Light> lights = new List<Light>();

    void Start()
    {
        windDirection.Normalize();
        stormHalfSize = transform.localScale / 2f;

        foreach(var light in lights)
        {
            light.enabled = false;
        }

        StartCoroutine(ThunderFlashes());
    }

    void Update()
    {
        StormMovement();
        BoundryCheck();
    }

    public void ChangeWindDirection(Vector2 dir)
    {
        windDirection = dir;
        windDirection.Normalize();
    }

    private void StormMovement()
    {
        Vector3 moveVec = new Vector3(windDirection.x * boderCorrection.x, 0, windDirection.y * boderCorrection.y) * speed * Time.deltaTime;
        transform.Translate(moveVec);

    }

    //checks whether the storm is over or hitting each boundry and adjusts the border corection vector acordingly
    public void BoundryCheck()
    {
        if (transform.position.x - stormHalfSize.x <= area.botLeftPos.x )
        {
            boderCorrection.x =  Mathf.Sign(windDirection.x);
        }
        if (transform.position.x + stormHalfSize.x >= area.topRightPos.x)
        {
            boderCorrection.x = Mathf.Sign(windDirection.x) * -1;
        }
        if (transform.position.z - stormHalfSize.z <= area.botLeftPos.z)
        {
            boderCorrection.y = Mathf.Sign(windDirection.y);
        }
        if (transform.position.z + stormHalfSize.z >= area.topRightPos.z)
        {
            boderCorrection.y = Mathf.Sign(windDirection.y) * -1;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boat"))
        {
            BoatController boat = other.GetComponent<BoatController>();
            boat.Wrecked();          
        }
    }

    IEnumerator ThunderFlashes()
    {
        WaitForSeconds flashLength = new WaitForSeconds(0.3f);

        while (true)
        {
            //choose a random light
            int index = Random.Range(0, lights.Count);

            //turn light on with random brightness
            lights[index].enabled = true;
            lights[index].intensity = Random.Range(0, 5);

            yield return flashLength;

            lights[index].enabled = false;

            yield return new WaitForSeconds(Random.Range(0.1f,0.2f));
        }
    }


}
