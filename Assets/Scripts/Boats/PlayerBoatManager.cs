using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerBoatManager : MonoBehaviour
{
    //scene camera
    public Camera cam;
    public LayerMask boatLayer;
    public LayerMask boatMovementLayer;

    //boat currently selected by the player
    private Boat selectedBoat;

    //functions will be attached an called when the player left or right clicks
    private UnityAction OnLeftClick;
    private UnityAction OnRightClick;

    private void Start()
    {
        OnLeftClick += GetBoatOnClick;
        OnRightClick += MoveSelectedBoat;

    }

    private void Update()
    {
        //When player uses mouse buttons invoke all functions attached to their events
        if (Input.GetMouseButtonDown(0)){
            OnLeftClick.Invoke();
        }
        if (Input.GetMouseButtonDown(1)){
            OnRightClick.Invoke();
        }
        

    }

    /// <summary>
    /// checks for a boat where the players cursor is and updates selected boat when there is
    /// </summary>
    private void GetBoatOnClick()
    {
        //raycast from cameras world position in the direction of the mouse
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit, 20, boatLayer))
        {
            if(selectedBoat != null) { selectedBoat.ToggleSelected(false); }

            selectedBoat = hit.transform.parent.GetComponent<Boat>();
            selectedBoat.ToggleSelected(true);
        }
    }

    private void MoveSelectedBoat()
    {
        if(selectedBoat == null) { return; }

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit, 20, boatMovementLayer))
        {
            Vector3 hitPos = hit.point;
            selectedBoat.MoveBoat(hitPos);
        }

    }

}
