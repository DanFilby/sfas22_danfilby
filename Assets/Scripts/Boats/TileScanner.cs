using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileScanner : MonoBehaviour
{
    //class refrences
    public Board oceanBoard;
    private BoatController boatcontroller;
    public GameController gameController;

    public float unknownScanTime;
    private IEnumerator interationRoutine;

    public int OilDrilledPerSecond = 10;
    private int totalOilCollected = 0;
    private int oilDiscorvered = 0;

    public Canvas boatHUD;
    private Vector3 HUDOffset;
    public Slider progressSlider;
    private Image sliderImage;

    private bool updateTextureOnce;
    private OceanTile tempTile;

    public TMPro.TMP_Text oilFoundText;

    void Start()
    {
        boatcontroller = GetComponent<BoatController>();

        HUDOffset = boatHUD.transform.localPosition;
        boatHUD.transform.parent = null;
        boatHUD.enabled= false;
        sliderImage = progressSlider.GetComponentInChildren<Image>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InteractWithTile();
           
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            if(interationRoutine != null)
            {
                StopCoroutine(interationRoutine);

                //if the oil drill coroutine finsihes without updating the texture use this
                //faster than updating the texture after each increment of oil drilled
                if (updateTextureOnce)
                {
                    oceanBoard.RevealTile(tempTile);
                    updateTextureOnce = false;
                }
            }
            boatcontroller.interacting = false;
            boatHUD.enabled = false;
        }

        
    }


    private void InteractWithTile()
    {
        //find the tile the boat is on
        OceanTile tile = oceanBoard.GetTile(transform.position);

        //scan any unkown tile or known regular tile
        if (!tile.Known || !tile.IsOil)
        {
            interationRoutine = ScanUnknownTile(tile);
        }
        else if (tile.IsOil)
        {
            interationRoutine = DrillOil(tile);
        }

        StartCoroutine(interationRoutine);
        boatcontroller.interacting = true;
    }


    private IEnumerator ScanUnknownTile(OceanTile tile)
    {
        boatHUD.enabled = true;
        boatHUD.transform.position = transform.position + HUDOffset;
        sliderImage.color = Color.white;

        WaitForSeconds delay = new WaitForSeconds(unknownScanTime / 100.0f);

        for (int i = 0; i < 100; i++)
        {
            progressSlider.value = i / 100.0f;
            yield return delay;
        }

        oceanBoard.RevealTile(tile);
        boatHUD.enabled = false;

        if (tile.IsOil) { oilDiscorvered++; }
        gameController.UpdateScore(oilDiscorvered, totalOilCollected);

        oilFoundText.text = $"Oil Found {oilDiscorvered}/10";
    }

    private IEnumerator DrillOil(OceanTile tile)
    {
        boatHUD.enabled = true;
        boatHUD.transform.position = transform.position + HUDOffset;
        sliderImage.color = Color.black;
        progressSlider.value = tile.OilAmount / 100.0f;

        //update the texture once after the couroutine
        updateTextureOnce = true;
        tempTile = tile;

        WaitForSeconds delay = new WaitForSeconds(0.5f);

        while(tile.OilAmount > 0)
        {
            yield return delay;

            totalOilCollected += OilDrilledPerSecond;
            tile.OilAmount -= OilDrilledPerSecond;

            progressSlider.value = tile.OilAmount / 100.0f;

            gameController.UpdateScore(oilDiscorvered, totalOilCollected);
        }

        yield return null;
    }

}

