using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boat : MonoBehaviour
{
    public float moveSpeed;
    public GameObject selectedIcon;

    private IEnumerator movingCoroutine;

    private void Start()
    {
        selectedIcon.SetActive(false);

    }

    public void MoveBoat(Vector3 targetPosition)
    {
        targetPosition.y = 0.5f;

        //stop the boat from moving to the previous point 
        if(movingCoroutine != null) { StopCoroutine(movingCoroutine); }
        
        //assign the current location to a move coroutine and start it 
        movingCoroutine = Move(targetPosition);
        StartCoroutine(movingCoroutine);
    }

    public void ToggleSelected(bool state)
    {
        selectedIcon.SetActive(state);
    }

    private IEnumerator Move(Vector3 targetPos)
    {
        WaitForEndOfFrame delay = new WaitForEndOfFrame();
        Vector3 direction = targetPos - transform.position;
        Vector3 moveVec = direction.normalized * moveSpeed;

        while(Vector3.Distance(transform.position,targetPos) > 0.1f)
        {
            transform.Translate(moveVec * Time.deltaTime);
            yield return delay;
        }

    }


}
