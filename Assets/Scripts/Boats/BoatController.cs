using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatController : MonoBehaviour
{
    [Header("Boat Speeds")]
    public float speed = 5f;
    public float turnSpeed = 20f;

    [Header("Borders")]
    public OceanArea area;

    //when the boat turns it will tip slightly
    [Header("Tipping")]
    public Transform boatBody;
    public float tipPower = 10;
    public float maxRotateAmount = 25f;
    private float currentTip = 0.0f;

    //makes the boat ride the waves
    [Header("Bouyancy")]
    public OceanShaderManager oceanManager;
    public Transform boatHull;
    public Transform boatBow;
    public Transform oceanCentre;

    [Header("Scanning")]
    //when the boat is scanning or drilling a tile
    public bool interacting = false;

    [Header("Storm")]
    public int hitTimePenalty = 10;  //seconds spent in spawn
    public int invincibletime = 5; //seconds invincible after coming out of spawn
    private Vector3 spawnPoint = new Vector3();
    private bool restrictToSpawn = false;
    private bool invincible = false;


    private void Start()
    {
        spawnPoint = transform.position;
    }

    void Update()
    {
        //find if the player is turning left or right 
        float turning = Input.GetAxis("Horizontal");

        //stop movement when the boat is scanning
        if (!interacting && !restrictToSpawn)
        {
            BoatMovement(turning);
            BoatTipping(turning);
        }
        
        UpdateBouyancy();
        BoatBounds();
    }

    //controls the boats movement and player input
    private void BoatMovement(float turning)
    {
        //find if the player is pressing w or s and get value -1 to 1
        float forward = Input.GetAxis("Vertical");
        float backwardSpeedChange = 0.5f;

        //only turn when the boat is moving forward
        if (forward != 0)
        {
            //move the boat forwards
            transform.Translate(Vector3.forward * forward * speed * ((forward < 0)? backwardSpeedChange : 1) * Time.deltaTime);

            //rotate the boat left or right
            transform.Rotate(new Vector3(0, 1, 0) * turning * forward * turnSpeed * Time.deltaTime);

            //find the tip amount and rotate the
            float tipAmount = turning * tipPower * Time.deltaTime;

            //make sure that the tip doesn't exceed the max amount
            if (currentTip <= maxRotateAmount && currentTip >= -maxRotateAmount)
            {
                boatBody.Rotate(new Vector3(0, 0, 1) * tipAmount);
                currentTip += tipAmount;
            }
        }
    }

    //controls the boat rocking side to side when turning
    private void BoatTipping(float turning)
    {
        //when the boat isn't turing rotate back to normal
        if (turning == 0 && currentTip != 0)
        {
            //find the rotate amount which is multipled by -1 when the current tip is over 1
            float readjustingTip = tipPower * Time.deltaTime * ((currentTip > 0) ? -1 : 1);
            boatBody.Rotate(new Vector3(0, 0, 1) * readjustingTip);

            currentTip += readjustingTip;
        }
    }


    private void UpdateBouyancy()
    {
        //find the ocean offset at the hull and front of the boat
        Vector3 hullWavePos = oceanManager.GetWaveOffset(transform.position - oceanCentre.position) + boatHull.localPosition;
        Vector3 bowWavePos = oceanManager.GetWaveOffset(transform.position - oceanCentre.position + boatBow.position);

        //change the position of the boat's body to where the wave is offseted with the hull position (lowers bottom of boat to the wave)
        boatBody.localPosition = hullWavePos;

        //find difference of position of the  waves at the middle and front of the boat
        Vector3 dif = bowWavePos - hullWavePos;

        //calculate the tilt angle
        float frontTilt = Mathf.Atan2(dif.y , dif.x);

        //set tilt angle
        Vector3 boatRot = boatBody.rotation.eulerAngles;
        boatRot.x = Mathf.Lerp(boatRot.x, -frontTilt, 1f);
        boatBody.rotation = Quaternion.Euler(boatRot);
    }

    /// <summary>
    /// Keeps the boat within the ocean borders
    /// </summary>
    private void BoatBounds()
    {
        //clamp the position to the min and max from the area variable
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, area.botLeftPos.x, area.topRightPos.x)
            , transform.position.y
            , Mathf.Clamp(transform.position.z, area.botLeftPos.z, area.topRightPos.z));

    }

    /// <summary>
    /// When the boat is struck by the strom and 'destroyed'. resets to spawn and gives a time penalty 
    /// </summary>
    public void Wrecked()
    {
        //check if already in spawn or in invincible time
        if(!restrictToSpawn && !invincible)
        {
            StartCoroutine(Respawn());
        }
    }

    private IEnumerator Respawn()
    {
        //move boat to spawn point
        transform.position = spawnPoint;
        restrictToSpawn = true;
        invincible = true;

        Renderer rend = gameObject.GetComponentInChildren<Renderer>();
        Color boatColour = rend.material.color;

        //the effect will alternate the boats colours to show its invincible
        Color clearColour = boatColour;
        clearColour.g = 1;

        //give time penalty for a set amount of seconds
        for (int i = 0; i < hitTimePenalty; i++)
        {
            if(i % 2 == 0)
            {
                rend.material.color = clearColour;
            }
            else
            {
                rend.material.color = boatColour;
            }

            yield return new WaitForSeconds(1f);
        }

        restrictToSpawn = false;

        //give some time for the player to go somewhere
        yield return new WaitForSeconds(invincibletime);

        //resset boat to normal
        rend.material.color = boatColour;
        invincible = false;
    }


}
