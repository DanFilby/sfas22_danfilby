using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


[System.Serializable]
public struct OceanArea
{
    public Transform botLeft;
    public Transform TopRight;

    public Vector3 botLeftPos { get { return botLeft.position; } }
    public Vector3 topRightPos { get { return TopRight.position; } }
}

public class Board : MonoBehaviour
{
    public enum Event { ClickedBlank, ClickedNearDanger, ClickedDanger, Win };

    [Header("Mine Sweeper")]
    [SerializeField] private Box BoxPrefab;
    [SerializeField] private int Width = 10;
    [SerializeField] private int Height = 10;
    [SerializeField] private int NumberOfDangerousBoxes = 10;

    private Box[] _grid;
    private Vector2Int[] _neighbours;
    private RectTransform _rect;
    private Action<Event> _clickEvent;

    [Header("Ocean Tiles")]
    [SerializeField] private int OceanSize = 10;
    public int NumberOfOilTiles;

    private OceanTile[] _oceanGrid;
    private Vector2Int[] _oceanNeighbours;

    [SerializeField] OceanArea OceanWorldSpace;
    private float _tileWidthWorldSpace;

    [SerializeField] List<Texture2D> NumberBitmaps;
    private Texture2D _tileMap;
    private int tilePixelSize = 100; //how big a tile will be in the texture
    public OceanShaderManager shaderManager;

    private void Awake()
    {
        //create ocean grid with 
        _oceanGrid = new OceanTile[OceanSize * OceanSize];

        _neighbours = new Vector2Int[8]
        {
            new Vector2Int(-OceanSize - 1, -1),
            new Vector2Int(-OceanSize, -1),
            new Vector2Int(-OceanSize + 1, -1),
            new Vector2Int(-1, 0),
            new Vector2Int(1, 0),
            new Vector2Int(OceanSize - 1, 1),
            new Vector2Int(OceanSize, 1 ),
            new Vector2Int(OceanSize + 1, 1)
        };

        for (int row = 0; row < OceanSize; ++row)
        {
            for (int column = 0; column < OceanSize; ++column)
            {
                int index = row * OceanSize + column;
                _oceanGrid[index] = new OceanTile();
                _oceanGrid[index].Setup(index, row, column);
            }
        }
       
        _tileWidthWorldSpace = (OceanWorldSpace.topRightPos.x - OceanWorldSpace.botLeftPos.x) / OceanSize;
        RechargeOceanTiles();

        CreateTileMapTexture();
    }

    /// <summary>
    /// Resets all tiles and sets up oil locations 
    /// </summary>
    public void RechargeOceanTiles()
    {
        //create list to represent all tiles 
        int numberOfItems = OceanSize * OceanSize;
        List<bool> dangerList = new List<bool>(numberOfItems);

        //populate list
        for (int count = 0; count < numberOfItems; ++count)
        {
            //add as many true values as number of oil tiles
            dangerList.Add(count < NumberOfOilTiles);
        }

        dangerList.RandomShuffle();

        //set each tile with number of neighbours and whether its an oil well
        for (int row = 0; row < OceanSize; ++row)
        {
            for (int column = 0; column < OceanSize; ++column)
            {
                int index = row * OceanSize + column;
                _oceanGrid[index].Charge(CountDangerNearby(dangerList, index), dangerList[index], 100);
            }
        }
    }

    public OceanTile GetTile(Vector3 worldPos)
    {
        return _oceanGrid[TileIndex(worldPos)];
    }

    public void RevealTile(OceanTile tile)
    {
        int tileIndex = tile.ID;
        ClearNearbyBlanks(_oceanGrid[tileIndex]);
        UpdateTileMapTexture();
    }

    /// <summary>
    /// Gets tile index from world positon
    /// </summary>
    private int TileIndex(Vector3 worldPos)
    {
        //find offset from the bottom left of the ocean 
        Vector3 offset = worldPos - OceanWorldSpace.botLeftPos;

        //find row and column indexs using the tile width
        int rowIndex = (int)(offset.z / _tileWidthWorldSpace);
        int columnIndex = (int)(offset.x / _tileWidthWorldSpace);

        return rowIndex * OceanSize + columnIndex;
    }

    /// <summary>
    /// Creates the tile map texture to show where each oil tile is and add the number of oil neighbours to regular tiles 
    /// </summary>
    private void CreateTileMapTexture()
    {
        _tileMap = new Texture2D(OceanSize * tilePixelSize, OceanSize * tilePixelSize, TextureFormat.RGB24, false);
        UpdateTileMapTexture();

        shaderManager.SetOceanTexture(_tileMap);
    }

    /// <summary>
    /// Goes over every tile in the ocean grid and updates the tile map texture to a grid of tiles each 
    /// showing the ocean tiles info I.E empty, oil, num oil neighbours
    /// </summary>
    private void UpdateTileMapTexture()
    {
        //row and column indexes
        int row = 0;
        int col = 0;

        //loop through each tile in the grid and colour the pixel block accordingly
        foreach (OceanTile tile in _oceanGrid)
        {
            //create the 100 by 100 pixel tile
            //if its got an oil neighbour write the number into the texture
            if (tile.DangerNearby > 0 && !tile.IsOil && tile.Known)
            {
                //a texture 100 by 100 texture of the number 
                Texture2D numTexture = NumberBitmaps[tile.DangerNearby - 1];

                //iterate over the number texture and where its black adda a black pixel to the tile map texture
                for (int x = 0; x < tilePixelSize; x++)
                {
                    for (int y = 0; y < tilePixelSize; y++)
                    {
                        _tileMap.SetPixel((col * tilePixelSize) + x, (row * tilePixelSize) + y, numTexture.GetPixel(x, y));
                    }
                }
            }
            else
            {
                //if its an oil the whole tile will be as black as there is oil in it, if not its white
                Color c = (tile.IsOil && tile.Known) ? Color.Lerp(Color.white, Color.black, Mathf.Max(0.2f, tile.OilAmount / 100.0f)) : Color.white;
                for (int x = 0; x < tilePixelSize; x++)
                {
                    for (int y = 0; y < tilePixelSize; y++)
                    {
                        _tileMap.SetPixel((col * tilePixelSize) + x, (row * tilePixelSize) + y, c);
                    }
                }
            }

            col++;
            //once we get to the end of the column start on the next row
            if (col == OceanSize)
            {
                col = 0;
                row++;
            }
        }
        _tileMap.Apply();
    }

#if UNITY_EDITOR
    //saves the texture to assess
    private void SaveTexture(Texture2D texture, string name)
    {
        byte[] bytes = texture.EncodeToPNG();
        var dirPath = "Assets/Textures/";
        File.WriteAllBytes(dirPath + name + ".png", bytes);
        UnityEditor.AssetDatabase.Refresh();
    }
#endif

    private void Update()
    {
        //Debug.Log(TileIndex(tester.transform.position));
    }

    private void ClearNearbyBlanks(OceanTile oceanTile)
    {
        RecursiveClearBlanks(oceanTile);
    }

    private void RecursiveClearBlanks(OceanTile oceanTile)
    {
        //player wants to find oil so set it as know as well
        oceanTile.Known = true;

        if (!oceanTile.IsOil)
        {
            if (oceanTile.DangerNearby == 0)
            {
                for (int count = 0; count < _neighbours.Length; ++count)
                {
                    int neighbourIndex = oceanTile.ID + _neighbours[count].x;
                    int expectedRow = oceanTile.RowIndex + _neighbours[count].y;
                    int neighbourRow = neighbourIndex / Width;
                    bool correctRow = expectedRow == neighbourRow;
                    bool active = neighbourIndex >= 0 && neighbourIndex < _oceanGrid.Length && !_oceanGrid[neighbourIndex].Known;

                    if (correctRow && active)
                    {
                        RecursiveClearBlanks(_oceanGrid[neighbourIndex]);
                    }
                }
            }
        }
    }



    #region exsiting Code



    public void Setup(Action<Event> onClickEvent)
    {
        _clickEvent = onClickEvent;
        Clear();
    }

    public void Clear()
    {
        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + column;
                _grid[index].StandDown();
            }
        }
    }

    public void RechargeBoxes()
    { 
        int numberOfItems = Width * Height;
        List<bool> dangerList = new List<bool>(numberOfItems);

        for (int count = 0; count < numberOfItems; ++count)
        {
            dangerList.Add(count < NumberOfDangerousBoxes);
        }

        dangerList.RandomShuffle();

        for (int row = 0; row < Height; ++row)
        {
            for (int column = 0; column < Width; ++column)
            {
                int index = row * Width + column;
                _grid[index].Charge(CountDangerNearby(dangerList, index), dangerList[index], OnClickedBox);
            }
        }
    }

    private void AwakeOld()
    {
        _grid = new Box[Width * Height];
        _rect = transform as RectTransform;
        RectTransform boxRect = BoxPrefab.transform as RectTransform;

        _rect.sizeDelta = new Vector2(boxRect.sizeDelta.x * Width, boxRect.sizeDelta.y * Height);
        Vector2 startPosition = _rect.anchoredPosition - (_rect.sizeDelta * 0.5f) + (boxRect.sizeDelta * 0.5f);
        startPosition.y *= -1.0f;

        _neighbours = new Vector2Int[8]
        {
            new Vector2Int(-Width - 1, -1),
            new Vector2Int(-Width, -1),
            new Vector2Int(-Width + 1, -1),
            new Vector2Int(-1, 0),
            new Vector2Int(1, 0),
            new Vector2Int(Width - 1, 1),
            new Vector2Int(Width, 1 ),
            new Vector2Int(Width + 1, 1)
        };

        for (int row = 0; row < Width; ++row)
        {
            GameObject rowObj = new GameObject(string.Format("Row{0}", row), typeof(RectTransform));
            RectTransform rowRect = rowObj.transform as RectTransform;
            rowRect.SetParent(transform);
            rowRect.anchoredPosition = new Vector2(_rect.anchoredPosition.x, startPosition.y - (boxRect.sizeDelta.y * row));
            rowRect.sizeDelta = new Vector2(boxRect.sizeDelta.x * Width, boxRect.sizeDelta.y);
            rowRect.localScale = Vector2.one;

            for (int column = 0; column < Height; ++column)
            {
                int index = row * Width + column;
                _grid[index] = Instantiate(BoxPrefab, rowObj.transform);
                _grid[index].Setup(index, row, column);
                RectTransform gridBoxTransform = _grid[index].transform as RectTransform;
                _grid[index].name = string.Format("ID{0}, Row{1}, Column{2}", index, row, column);
                gridBoxTransform.anchoredPosition = new Vector2( startPosition.x + (boxRect.sizeDelta.x * column), 0.0f);
            }
        }

        // Sanity check
        for(int count = 0; count < _grid.Length; ++count)
        {
            Debug.LogFormat("Count: {0}  ID: {1}  Row: {2}  Column: {3}", count, _grid[count].ID, _grid[count].RowIndex, _grid[count].ColumnIndex);
        }
    }

    private int CountDangerNearby(List<bool> danger, int index)
    {
        int result = 0;
        int boxRow = index / Width;

        if (!danger[index])
        {
            for (int count = 0; count < _neighbours.Length; ++count)
            {
                int neighbourIndex = index + _neighbours[count].x;
                int expectedRow = boxRow + _neighbours[count].y;
                int neighbourRow = neighbourIndex / Width;
                result += (expectedRow == neighbourRow && neighbourIndex >= 0 && neighbourIndex < danger.Count && danger[neighbourIndex]) ? 1 : 0;
            }
        }

        return result;
    }


    private void OnClickedBox(Box box)
    {
        Event clickEvent = Event.ClickedBlank;

        if(box.IsDangerous)
        {
            clickEvent = Event.ClickedDanger;
        }
        else if(box.DangerNearby > 0)
        {
            clickEvent = Event.ClickedNearDanger;
        }
        else
        {
            ClearNearbyBlanks(box);
        }

        if(CheckForWin())
        {
            clickEvent = Event.Win;
        }

        _clickEvent?.Invoke(clickEvent);
    }

    private bool CheckForWin()
    {
        bool Result = true;

        for( int count = 0; Result && count < _grid.Length; ++count)
        {
            if(!_grid[count].IsDangerous && _grid[count].IsActive)
            {
                Result = false;
            }
        }

        return Result;
    }

    private void ClearNearbyBlanks(Box box)
    {
        RecursiveClearBlanks(box);
    }

    private void RecursiveClearBlanks(Box box)
    {
        if (!box.IsDangerous)
        {
            box.Reveal();

            if (box.DangerNearby == 0)
            {
                for (int count = 0; count < _neighbours.Length; ++count)
                {
                    int neighbourIndex = box.ID + _neighbours[count].x;
                    int expectedRow = box.RowIndex + _neighbours[count].y;
                    int neighbourRow = neighbourIndex / Width;
                    bool correctRow = expectedRow == neighbourRow;
                    bool active = neighbourIndex >= 0 && neighbourIndex < _grid.Length && _grid[neighbourIndex].IsActive;

                    if (correctRow && active)
                    {
                        RecursiveClearBlanks(_grid[neighbourIndex]);
                    }
                }
            }
        }
    }
}
#endregion 