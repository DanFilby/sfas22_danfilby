using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A wave used in the ocean shader which follows the gerstner model to create an ocean effect
/// </summary>
[System.Serializable]
public struct OceanWave
{
    public Vector2 direction;

    [Range(0,1)]
    public float steepness;
    public float waveLength;
    
    //Plugs into the shader as a vector
    public Vector4 wave { get { return new Vector4(direction.x, direction.y, steepness, waveLength); } }

}
public class OceanShaderManager : MonoBehaviour
{
    //this has the size of the mesh
    private OceanMeshGenerator oceanMeshGenerator;
    private Material oceanMat;

    public List<OceanWave> waves;

    //local variable to sync time between shader and script
    private float realTime;

    void Awake()
    { 
        //get the mesh generator and setup the shader
        oceanMeshGenerator = GetComponent<OceanMeshGenerator>();
        oceanMat = gameObject.GetComponent<Renderer>().material;

        //find the width of the mesh and create a square texture with it
        int oceanMeshWidth = oceanMeshGenerator.size * oceanMeshGenerator.meshDensity + 1;

        SetShaderProperties();
    }

    private void Update()
    {
        //sync time between shader and script
        realTime += Time.deltaTime;
        oceanMat.SetFloat("_RealTime", realTime);

        //update the wave properties 
        //SetShaderProperties();
    }

    public void SetOceanTexture(Texture2D texture)
    {
        oceanMat.mainTexture = texture;
    }

    public Texture GetOceanTexture()
    {
        return oceanMat.mainTexture;
    }

    private void SetShaderProperties()
    {
        //set up the waves in the shader
        oceanMat.SetVector("_WaveA", waves[0].wave);
        oceanMat.SetVector("_WaveB", waves[1].wave);
        oceanMat.SetVector("_WaveC", waves[2].wave);
    }

    /// <summary>
    /// returns the offset of all the current waves in the ocean
    /// </summary>
    public Vector3 GetWaveOffset(Vector3 worldPos)
    {
        Vector3 offset = new Vector3();
        foreach(OceanWave wave in waves)
        {
            offset += GerstnerWaveHeight(wave, new Vector2(worldPos.x, worldPos.z));
        }

        return offset;
    }

    //gets the postion offset of all the waves
    private Vector3 GerstnerWaveHeight(OceanWave wave, Vector2 vertexPos)
    {
        float k = 2 * Mathf.PI / wave.waveLength;
        float c = Mathf.Sqrt(9.8f / k);                               
        Vector2 d = (wave.direction).normalized; ;                      
        float f = k * (Vector2.Dot(d, vertexPos) - c * realTime);           
        float a = wave.steepness / k;

        return new Vector3(d.x * (a * Mathf.Cos(f)), a * Mathf.Sin(f), d.y * (a * Mathf.Cos(f)));
    }




}
