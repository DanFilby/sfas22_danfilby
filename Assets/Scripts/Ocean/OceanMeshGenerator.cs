using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class OceanMeshGenerator : MonoBehaviour
{
    //adds to gameobject to render mesh 
    private MeshFilter meshFilter;
    
    public Material oceanMat;

    [Tooltip("")]
    public int meshDensity = 1;
    public int size = 15;

#if UNITY_EDITOR
    //able to be called in editor to create and save the plane mesh
    [ContextMenu("Generate and Save Mesh")]
    private void SaveMesh()
    {
        meshFilter = gameObject.GetComponent<MeshFilter>();

        //generate mesh and add to mesh filter
        Mesh oceanMesh = GenerateMesh();
        meshFilter.mesh = oceanMesh;

        //save the mesh
        AssetDatabase.CreateAsset(oceanMesh, "Assets/Models/Generated/OceanMesh.mesh");
        AssetDatabase.SaveAssets();
    }
#endif

    //generates a plane mesh with a given density and size
    private Mesh GenerateMesh()
    {
        //mesh density has to be even
        if(meshDensity % 2 == 1) { meshDensity += 1; }

        Mesh mesh = new Mesh();

        List<Vector3> verts = new List<Vector3>();
        List<int> tris = new List<int>();
        List<Vector2> uvs = new List<Vector2>();

        //how far apart each vertex will be
        float vertSpacing = 1.0f / meshDensity;

        float xPos = 0, yPos = 0;

        //counter for how many verts will be in a row
        int vertsInRow = 0;

        //loop through the size in x and y axis and create a vertex at each point
        while (xPos <= size)
        {
            while (yPos <= size)
            {
                verts.Add(new Vector3(xPos - size / 2, 0, yPos - size / 2));
                yPos += vertSpacing;
            }
            //verts.Add(new Vector3(xPos - size / 2, 0, size / 2));
            yPos = 0;
            xPos += vertSpacing;

            vertsInRow++;
        }

        //create square from two tris at each vertex except last row
        for (int i = 0; i < verts.Count - vertsInRow - 1; i++)
        {
            tris.Add(i);
            tris.Add(i + 1);
            tris.Add(vertsInRow + i);

            tris.Add(vertsInRow + i);
            tris.Add(i + 1);
            tris.Add(vertsInRow + i + 1);
        }

        //+ 1 because one more vertex to reach desired size
        float width = size * meshDensity + 1;

        //Caclulate uv coords of the plane from the index of the vertex in the list 
        for (int i = 0; i < verts.Count; i++)
        {
            //find column and row index then / by width -1 to get in range of 0 - 1
            uvs.Add(new Vector2(((int)(i / width)) / (width - 1), (i % width) / (width - 1)));
        }

        //add to mesh and calculate normals
        mesh.vertices = verts.ToArray();
        mesh.triangles = tris.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.RecalculateNormals();

        return mesh;
    }

}
