using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OceanTile
{
    public int RowIndex { get; private set; }
    public int ColumnIndex { get; private set; }
    public int ID { get; private set; }
    public int DangerNearby { get; private set; }
    public bool IsOil { get; private set; }
    public bool Known { get; set; }
    public int OilAmount { get; set; }

    public void Setup(int id, int row, int column)
    {
        ID = id;
        RowIndex = row;
        ColumnIndex = column;
    }

    public void Charge(int dangerNearby, bool oil, int oilAmount)
    {
        DangerNearby = dangerNearby;
        IsOil = oil;
        Known = false;
        OilAmount = oilAmount;
    }

}
